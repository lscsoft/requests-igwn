requests-igwn Documentation
===========================

requests-igwn provides HTTP authentication for International Gravitational-Wave
Observatory Network (IGWN) services. It uses the powerful `Requests`_ package
for reliable and high-throughput HTTP connection pooling.

Quick Start
-----------

Install with pip_::

    pip install requests-igwn

API
---

.. automodule:: requests_igwn
.. automodule:: requests_igwn.auth
.. automodule:: requests_igwn.cert_reload
.. automodule:: requests_igwn.errors
.. automodule:: requests_igwn.file
.. automodule:: requests_igwn.user_agent

.. _Requests: http://requests.readthedocs.io/
.. _pip: http://pip.pypa.io
