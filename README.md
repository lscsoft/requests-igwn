# requests-igwn

requests-igwn provides HTTP authentication for International Gravitational-Wave
Observatory Network (IGWN) services. It uses the powerful [Requests] package
for reliable and high-throughput HTTP connection pooling.

[Requests]: http://requests.readthedocs.io/

## Notes for software packaging

Software packaging files exist for the following systems:

- RPM: https://git.ligo.org/packaging/rhel/python-requests-igwn
- Debian: https://git.ligo.org/packaging/debian/requests-igwn
- Conda: https://github.com/conda-forge/requests-igwn-feedstock
